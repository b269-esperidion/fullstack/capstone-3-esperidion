import { useState } from 'react';
import CreateProductForm from '../components/CreateProductForm';
import AllProducts from '../components/AllProducts';


export default function AdminDashboard() {
  const [showCreateForm, setShowCreateForm] = useState(false);

  // retrieve token from local storage
  const token = localStorage.getItem('token');

  const handleCreateButtonClick = () => {
    setShowCreateForm(true);
  }

  const handleCancelButtonClick = () => {
    setShowCreateForm(false);
  }




  return (
    <> <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' , marginTop: "10px" }} className= "bg-light" >
      <h1 >Admin Dashboard</h1>
      <button type="button" onClick={handleCreateButtonClick}>Create Product</button>
      {showCreateForm && (
        <>
          <CreateProductForm token={token} />
          <button type="button" onClick={handleCancelButtonClick} style={{width: "195px", marginTop: "5px"}}>Cancel</button>
        </>
      )}
      </div>

        <AllProducts/>
    </>
  );
}
