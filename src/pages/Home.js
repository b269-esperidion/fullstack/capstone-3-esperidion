import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import UncontrolledExample from '../components/UncontrolledExample';


export default function Home() {

	const data = {
		title: "Cyber Haven",
		content: "Build No Limits",
		destination: "/products",
		label: "Buy now!"
	}
 

	return (
		<>

		<Banner data={data} />
    	<Highlights />
    	<UncontrolledExample/>
    	
    	
		</>
	)
}


