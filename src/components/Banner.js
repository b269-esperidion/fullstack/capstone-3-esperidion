
import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}) {

    const {title, content, destination, label} = data;

return (
    <Row style={{display: 'flex', flexDirection: 'column'}}>
        <Col className="p-5" style={{color: "white"}} >
            <h1 >{title}</h1>
            <p>{content}</p>
            {/*<Link to={destination}>{label}</Link>*/}
           <Button variant="primary" as={Link} to={destination} style={{ background: 'linear-gradient(#00b8fc, #000000)' }}>
  {label}
</Button>

        </Col>
    </Row>
    )
}


