import { useState, useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import { FaBars } from 'react-icons/fa';

export default function AppNavbar() {
  const { user } = useContext(UserContext);
  const [expanded, setExpanded] = useState(false);

  const toggleExpanded = () => {
    setExpanded(!expanded);
  };

  return (
    <Navbar
      bg="light"
      expand="lg"
      className="p-3"
      style={{
        background: 'linear-gradient(#00b8fc, #000000)',
        height: '80px',
        fontSize: '15px',
        fontWeight: 'bold',
        borderBottom: '2px solid white'
      }}
      expanded={expanded}
    >
      <Container>
        <Navbar.Brand as={Link} to="/" style={{ maxWidth: '170px' }}>
          <img
            src="https://www.harrisonclarke.com/hubfs/HarrisonClarke_Logo%20clients-Cyberheaven.png"
            alt="Cyber Haven Logo "
            style={{ height: 'auto', maxWidth: '100%' }}
          />
        </Navbar.Brand>
        <Navbar.Toggle
          aria-controls="basic-navbar-nav"
          onClick={toggleExpanded}
        >
          <FaBars />
        </Navbar.Toggle>
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto" onClick={toggleExpanded}>
            <Nav.Link as={NavLink} to="/" style={{textAlign: "right" , color: "white"}}>
              Home
            </Nav.Link>
            <Nav.Link as={NavLink} to="/products"style={{textAlign: "right" ,color: "white"}}>
              Product
            </Nav.Link>
            {user.isAdmin === true && (
              <Nav.Link as={NavLink} to="/admin"style={{textAlign: "right" ,color: "white"}}>
                Admin Dashboard
              </Nav.Link>
            )}
          </Nav>
          <Nav className="ml-auto" onClick={toggleExpanded}>
            {user.id !== null ? (
              <Nav.Link as={NavLink} to="/logout" style={{textAlign: "right" ,color: "white"}}>
                Logout
              </Nav.Link>
            ) : (
              <>
                <Nav.Link as={NavLink} to="/login" style={{textAlign: "right" ,color: "white"}}>
                  Login
                </Nav.Link>
                <Nav.Link as={NavLink} to="/register" style={{textAlign: "right" ,color: "white"}}>
                  Register
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
