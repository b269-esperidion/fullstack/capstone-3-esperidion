import { useState, useEffect } from 'react';
import Swal from 'sweetalert2';

export default function AllProducts() {
  const [products, setProducts] = useState([]);

const handleUpdateProduct = (productId, updatedProduct) => {
  const index = products.findIndex(product => product._id === productId);
  const currentProduct = products[index];

  if (JSON.stringify(currentProduct) === JSON.stringify(updatedProduct)) {
    // Display confirmation dialog
    Swal.fire({
      title: 'Are you sure?',
      text: 'You are about to update the product with the same data. Do you want to proceed?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, update it!',
      cancelButtonText: 'Cancel'
    }).then((result) => {
      if (result.isConfirmed) {
        // Proceed with update
        updateProduct(productId, updatedProduct);
      }
    });
  } else {
    // Proceed with update
    updateProduct(productId, updatedProduct);
  }
};
const updateProduct = (productId, updatedProduct) => {
  fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    },
    body: JSON.stringify(updatedProduct)
  })
    .then(res => res.json())
    .then(data => {
      console.log(`Product with id ${productId} updated successfully.`);
      // Reload product data
      fetch(`${process.env.REACT_APP_API_URL}/products/all`)
        .then(res => res.json())
        .then(data => {
          setProducts(data);
         
        })
        .catch(error => {
          console.error('Error fetching products:', error);
        });
    })
    .catch(error => {
      console.error(`Error updating product with id ${productId}:`, error);
      // Display error message
      Swal.fire({
        title: 'Error',
        text: 'There was an error updating the product. Please try again later.',
        icon: 'error'
      });
    });
};

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then(res => res.json())
      .then(data => {
        setProducts(data);
      })
      .catch(error => {
        console.error('Error fetching products:', error);
      });
  }, []);

  return (
  	<div style={{ textAlign: "center" }} className= "bg-light"  >
    <table style={{ margin: '0 auto',  fontSize: '20px' }} >
      <thead>
        <tr>
          <th>Name</th>
          <th>Description</th>
          <th>Price</th>
          <th>Status</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {products.map(product => (
          <tr key={product._id}>
            <td>
              <input
                type="text"
                value={product.name}
                onChange={e => {
                  const updatedProduct = {...product, name: e.target.value};
                  handleUpdateProduct(product._id, updatedProduct);
                }}
              />
            </td>
            <td>
              <input
                type="text"
                value={product.description}
                onChange={e => {
                  const updatedProduct = {...product, description: e.target.value};
                  handleUpdateProduct(product._id, updatedProduct);
                }}
              />
            </td>
            <td>
			  <input
			    type="number"
			    value={product.price}
			    onChange={e => {
			      const updatedProduct = {...product, price: e.target.value};
			      handleUpdateProduct(product._id, updatedProduct);
			    }}
			  />
			</td>
			<td>
 		 <button onClick={() => handleUpdateProduct(product._id, {...product, isActive: !product.isActive})}>
   		 {product.isActive ? 'Deactivate' : 'Activate'}
 		 </button>
			</td>
            <td>
              <button onClick={() => handleUpdateProduct(product._id, product)}>Update</button>
            </td>
          </tr>
        ))}
      </tbody>
    </table ></div>
  );
}
