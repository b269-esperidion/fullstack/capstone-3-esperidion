import { useState } from 'react';

export default function CreateProductForm({ token }) {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState(0);
  const [formStatus, setFormStatus] = useState(null);

  const handleSubmit = (e) => {
    e.preventDefault();
    const data = { name, description, price, quantity };
    fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify(data)
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      setFormStatus('success');
    })
    .catch(error => {
      console.error(error);
      setFormStatus('error');
    });
  };

   return (
    <form onSubmit={handleSubmit} style={{ display: 'flex', flexDirection: 'column' }}>

      <div style={{ display: 'flex', flexDirection: 'column', marginBottom: '10px' }}>
        <label style={{ marginBottom: '5px' }}>
          Name:
        </label>
        <input type="text" value={name} onChange={(e) => setName(e.target.value)} style={{ padding: '5px' }} />
      </div>
      <div style={{ display: 'flex', flexDirection: 'column', marginBottom: '10px' }}>
        <label style={{ marginBottom: '5px' }}>
          Description:
        </label>
        <textarea value={description} onChange={(e) => setDescription(e.target.value)} style={{ padding: '5px' }} />
      </div>
      <div style={{ display: 'flex', flexDirection: 'column', marginBottom: '10px' }}>
        <label style={{ marginBottom: '5px' }}>
          Price:
        </label>
        <input type="number" value={price} onChange={(e) => setPrice(e.target.value)} style={{ padding: '5px' }} />
      </div>
      <div style={{ display: 'flex', flexDirection: 'column', marginBottom: '10px' }}>
        <label style={{ marginBottom: '5px' }}>
          Quantity:
        </label>
        <input type="number" value={quantity} onChange={(e) => setQuantity(e.target.value)} style={{ padding: '5px' }} />
      </div>      {formStatus === 'success' && <div style={{ backgroundColor: 'green', color: 'white', padding: '10px', marginBottom: '10px' }}>Product created successfully!</div>}
      {formStatus === 'error' && <div style={{ backgroundColor: 'red', color: 'white', padding: '10px', marginBottom: '10px' }}>An error occurred while creating the product.</div>}
      <button type="submit" style={{ marginTop: '10px' }}>Create </button>
    </form>
  );
}