import Carousel from 'react-bootstrap/Carousel';

function UncontrolledExample() {
  return (
    <Carousel >
      <Carousel.Item>
        <img
          className="d-block w-100 "
          src="https://scontent.fmnl4-1.fna.fbcdn.net/v/t39.30808-6/336142972_229336699664999_2514943169820194317_n.jpg?_nc_cat=109&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeH3BzgAjFpqQv7QeJQiccQB4v53Hjny4k7i_nceOfLiTkhgLgub8I4_k9Sk6UlJ4DgbuZTqqOXTQl7RE8JSCKTu&_nc_ohc=yWgqkEK9zpwAX9U9im5&_nc_ht=scontent.fmnl4-1.fna&oh=00_AfDLxAp4rQv20BDT-XjtwjqWiXYAb_8UnEbmkGpn1BrA0Q&oe=64497322"
          alt="First slide"
           

        />
       
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://scontent.fmnl4-6.fna.fbcdn.net/v/t39.30808-6/335632407_531235932466922_980145101465367581_n.jpg?_nc_cat=100&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeFIAf7yeHVtWMj27u9f01JE9LyN68cEeoP0vI3rxwR6g8BAjBch-fh_hatYxZY3_WcVmkic1zzl6nVUijCi3Ass&_nc_ohc=WaUlwPl8198AX99nw_9&_nc_ht=scontent.fmnl4-6.fna&oh=00_AfBOiOvcVutJUh1pWxIH32yI-D0pTizV2aieItxrZcdrEQ&oe=644965FE"
          alt="Second slide"
           
        />

     
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://scontent.fmnl4-3.fna.fbcdn.net/v/t39.30808-6/335272084_2257732931101506_2993334803228832879_n.jpg?_nc_cat=110&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeHpE5uFkPwha5GdTT8pWoXqzdbeov4HhmDN1t6i_geGYLOdImoVNg6-vBm5-UtuFXfagX-2J8TQce7a-NJCOjjW&_nc_ohc=ywKGn-NHQSYAX-z3FdD&_nc_ht=scontent.fmnl4-3.fna&oh=00_AfDW6f8bnndCbjtOkYcJOqqCplCC_mHW5J6XRLzfz0QA4g&oe=6449D9E9"
          alt="Third slide"
           
        />


       
      </Carousel.Item>
    </Carousel>
  );
}

export default UncontrolledExample;
