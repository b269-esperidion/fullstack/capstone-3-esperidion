import { Row, Col, Card, h1} from 'react-bootstrap';

export default function Highlights() {
	return ( 
	    <Row className="mt-3 mb-3">
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3" className="mt-3 mb-3">
	            <Card.Img variant="top" src="https://scontent.fmnl4-1.fna.fbcdn.net/v/t39.30808-6/336363306_536091085322883_4934295302686998297_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeFEHEglILzaJRegXehZ1Un3zycHeR5KYcTPJwd5HkphxJVxoDz3aNP7fWmH9JxDEvp__u21s-5-dueVTKeHCl-h&_nc_ohc=VLnvNP-wo5oAX-11xBE&_nc_ht=scontent.fmnl4-1.fna&oh=00_AfAFSlt04SEQMh3YrqXVeZNP2adLNB_G8wfKv4g5v0WrIA&oe=644AEC58"/>
	               
	            </Card>
	        </Col>
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3" className="mt-3 mb-3">
	             <Card.Img variant="top" src="https://scontent.fmnl4-6.fna.fbcdn.net/v/t39.30808-6/335141369_583939917115691_8541902896051457370_n.jpg?_nc_cat=108&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeHBB-F8XqVUV1N_MoRP4b_GXINFqdiULUFcg0Wp2JQtQWHpydB_0RH1t6y0I7T8e0chUUoW7YWUN2LQoa3p0ghn&_nc_ohc=T7V9L4VoAXgAX9ccRMj&_nc_ht=scontent.fmnl4-6.fna&oh=00_AfDDtX9jBBWUXJUpyh1TpMp4lh9ThWuLj3ETfULQOxwsjg&oe=644B2482"/>
	              
	            </Card>
	        </Col>
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3" className="mt-3 mb-3">
	            <Card.Img variant="top" src="https://scontent.fmnl4-4.fna.fbcdn.net/v/t39.30808-6/336121212_1234837727124973_8798607441767871675_n.jpg?_nc_cat=102&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeHwY1pgxUtql5T5diIMitfNiQNsbrUMPRmJA2xutQw9Ge-pvNmV9iND_S0OLSgAB2BHIYqLFfJMD6kvjajZEKME&_nc_ohc=5Sm2GWY4LawAX8ogrC9&_nc_ht=scontent.fmnl4-4.fna&oh=00_AfATG4P3OgT92TaV_akH5fo14DYBiRpegQig8pF7rqqFTA&oe=644A79CF"/>
	               
	            </Card>
	        </Col>
	         <Col xs={12} md={4} >
	            <Card className="cardHighlight p-3" className="mt-3 mb-3">
	            <Card.Img variant="top" src="https://scontent.fmnl4-1.fna.fbcdn.net/v/t39.30808-6/335212012_198949319434200_8845614063441356747_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeGI5uZ6sNZOgFafzbM8OsaVIjuH70aPJPQiO4fvRo8k9GzAMqbsKi2IZceNDswGnqUetGdjsvCUeVaGLs6MIMUT&_nc_ohc=Sdr6XHon6jsAX-IAkE4&_nc_ht=scontent.fmnl4-1.fna&oh=00_AfADbH46-VTx7FvHjJ6zKOQqDWM088kZk-52qne-LTiWgA&oe=6449CAFD"/>
	               
	            </Card>
	        </Col>
	         <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3" className="mt-3 mb-3">
	            <Card.Img variant="top" src="https://scontent.fmnl4-2.fna.fbcdn.net/v/t39.30808-6/336073400_752871219702458_242612965345855570_n.jpg?_nc_cat=105&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeGV76u5thgwOAaQV9qnFYnCJBxy3FZ8a5wkHHLcVnxrnFXSzKAdm2UzTKKvYWZvswvP0-cvC54Ul1KkOU9nPj9j&_nc_ohc=-uRvtxyY-RcAX_DpmYw&_nc_ht=scontent.fmnl4-2.fna&oh=00_AfBYJ4K3pAcnPZnTGe8gTn6CFg3RlD2gpys5zKcrsIKUUQ&oe=644AA6F6"/>
	               
	            </Card>
	        </Col>
	         <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3" className="mt-3 mb-3">
	            <Card.Img variant="top" src="https://scontent.fmnl4-4.fna.fbcdn.net/v/t39.30808-6/335698371_1586834205156466_2575924016004898276_n.jpg?_nc_cat=102&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeFSk72Zu90xpEDt0LPOtaoZ2XLOkno_f8nZcs6Sej9_yUqIZ2zZL7kM6PnNxXFiu-cNgG_xGsK6KTizhwd_pnfp&_nc_ohc=hGnfXE5Qn5IAX8aPQ0a&_nc_ht=scontent.fmnl4-4.fna&oh=00_AfAW1T2cV7Fb2t1eVjXS2PlviON5zzvixGwDcO6DzlyV5Q&oe=64499137"/>
	               
	            </Card>
	        </Col>
	    </Row>
	)
}